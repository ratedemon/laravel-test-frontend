import { Component, OnInit } from '@angular/core';
import {DataService} from '../shared/data.service';
import {Response} from "@angular/http"


@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})
export class GamePageComponent implements OnInit {
  pets:any = [];
  private couter = 0;
  private counterOut = "";
  private creating = false;
  constructor(private dataService: DataService) { 
    dataService.messages.subscribe(data=>{this.pets = data;
      
  });
  }
  ngOnInit() {
    this.dataService.getAnimals().subscribe((data:Response)=>{console.log(data.json());this.pets=data.json()});
  }
  pagePlus(){
    this.couter++;
    if(this.couter>this.pets.length-1){
      this.couter = 0;
    }
    this.counterOut = "translateX(" +  -this.couter * 100 + "%)";
    console.log(this.couter);
  }
  pageMinus(){
    this.couter--;
    if(this.couter<0){
      this.couter = this.pets.length-1;
    }
    this.counterOut = "translateX(" +  -this.couter * 100 + "%)";
    console.log(this.couter);
  }
  setAnimal(id){
    this.dataService.addPet(id).subscribe(data=>console.log(data));  
    this.toggle();
  }
  // setDog(){
  //   this.dataService.addPet(1).subscribe(data=>console.log(data));    
  //   this.toggle();
  // }
  // setCat(){
  //   this.dataService.addPet(2).subscribe(data=>console.log(data)); ;    
  //   this.toggle();    
  // }
  // setRaccon(){
  //   this.dataService.addPet(3).subscribe(data=>console.log(data)); ;    
  //   this.toggle();
  // }
  toggle(){
    this.creating = !this.creating;
  }

}
