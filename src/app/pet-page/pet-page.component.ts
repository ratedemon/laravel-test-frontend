import { Component, OnInit, Input } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import {DataService} from '../shared/data.service';

@Component({
  selector: 'pet-page',
  templateUrl: './pet-page.component.html',
  styleUrls: ['./pet-page.component.scss']
})
export class PetPageComponent implements OnInit {
  @Input() pet;
  @Input() pets;
  private whatTime = Observable.interval(1000).map(x => new Date().getTime() - new Date(this.pet.animal_characteristics[0].last_up).getTime()).share();
  private sleepTime =Observable.interval(1000).map(x=>new Date().getTime() - new Date(this.pet.animal_characteristics[1].last_up).getTime()).share();
  private careTime =Observable.interval(1000).map(x=>new Date().getTime() - new Date(this.pet.animal_characteristics[2].last_up).getTime()).share();
  private happyTime =Observable.interval(1000).map(x=>new Date().getTime() - new Date(this.pet.animal_characteristics[3].last_up).getTime()).share();
  constructor(private dataService: DataService) {
  }
  ngOnInit() {

  } 
  getHungry(){
    return this.pet.animal_characteristics[0].value + "%";
  }
  getCare(){
    return this.pet.animal_characteristics[2].value + "%";
  }
  getSleep(){
    return this.pet.animal_characteristics[1].value + "%";
  }
  getHappy(){
    return this.pet.animal_characteristics[3].value + "%";
  }
  takeImage(){
    let pic = 'assets/image/';
    switch(this.pet.animal.id){
      case 1:
       pic += 'dog.svg';
       break;
      case 2:
       pic += 'cat.svg';
       break;
      case 3:
        pic += 'raccoon.svg'
        break;
      default:
        pic +='penguin.svg';
    }
    return pic;
  }
  toBetter(animal, idCharacteristic){
    let id = animal.animal.id;
    let characteristic = animal.animal_characteristics[idCharacteristic].id;
    // this.sendToService(id, characteristic);
    this.dataService.toBad(id, characteristic).subscribe(data=>console.log(data));
  }
  // sendToService(idAnimal, idCharacteristic){
    
  // }
}
