import {Router} from "@angular/router";
import {GamePageComponent} from "../game-page/game-page.component";
import {LoginPageComponent} from "../login-page/login-page.component";

export const appRoutes = [
  {path: 'login', component: LoginPageComponent},
  {path: '', component: GamePageComponent}
]