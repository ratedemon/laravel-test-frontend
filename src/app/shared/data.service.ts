import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import Pusher from 'pusher-js';

@Injectable()
export class DataService {
  private url = "http://localhost:8000/";
  // private socket;
  private _messages = new BehaviorSubject([]);
  public messages = this._messages.asObservable();
  private pusher;
  private channels = [];
  constructor(private http: Http) { 
    // this.getAnimals();
    this.pusher = new Pusher('73de5663fa65bd954c52', {
      cluster: 'eu',
      encrypted: true
    });
    this.channels = [];
    this.pusher.logToConsole = true;
    let channel = this.pusher.subscribe('user.1');
    channel.bind('App\\Events\\AnimalEvent', (data)=>{
      console.log(data);
      this._messages.next(data);
    });
  }
  addPet(id){
    let headers = new Headers();
    // this.createAuthorizationHeader(headers);
    return this.http.get(this.url+'api/animal/'+id , {headers: headers}).map(x=>console.log(x));
  }
  private createAuthorizationHeader(headers: Headers){
    headers.append("Content-Type", "application/json" ); 
  }
  toBad(idAnimal: number, idCharacteristic: number){
    let headers = new Headers();
    // console.log(idAnimal, idCharacteristic);
    // this.createAuthorizationHeader(headers);
    return this.http.get(this.url+'api/animal/'+idAnimal+'/'+idCharacteristic, {headers: headers}).map(x=>console.log(x));
  }
  getAnimals(){
    return this.http.get(this.url+'api/animals');
  }
}
