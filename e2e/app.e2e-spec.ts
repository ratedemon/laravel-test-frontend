import { TamagotchiPage } from './app.po';

describe('tamagotchi App', () => {
  let page: TamagotchiPage;

  beforeEach(() => {
    page = new TamagotchiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
